using BasicApi;
using Xunit;

namespace BasicApiTests
{
    public class Tests
    {
        [Fact]
        public void Test1()
        {
            var weather = new WeatherForecast();

            weather.TemperatureC = 30;

            Assert.Equal(85, weather.TemperatureF);
        }
    }
}